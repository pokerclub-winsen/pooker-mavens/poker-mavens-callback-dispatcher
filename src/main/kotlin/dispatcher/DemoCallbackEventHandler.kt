package dispatcher

import dispatcher.event.CallbackEvent
import org.springframework.context.event.EventListener
import org.springframework.stereotype.Component

@Component
class DemoCallbackEventHandler {

    @EventListener(classes = [CallbackEvent::class])
    fun printOnCallbackEvent(event: CallbackEvent) {
        println("*** Received event: $event")
    }

}