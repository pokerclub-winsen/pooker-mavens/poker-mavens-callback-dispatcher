package dispatcher

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class PokerMavensCallbackDispatcherApplication {

	@Bean
	fun objectMapper(): ObjectMapper {
		return jacksonObjectMapper().apply {
			registerModule(JavaTimeModule())
		}
	}
}

fun main(args: Array<String>) {
    runApplication<PokerMavensCallbackDispatcherApplication>(*args)
}
