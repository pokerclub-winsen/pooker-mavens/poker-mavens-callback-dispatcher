package dispatcher.event

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.time.LocalDateTime


@JsonIgnoreProperties(value = ["Password", "Event"])
data class NewAccountEvent(
    val Player: String,
    val Source: Source,
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    val Time: LocalDateTime,
    val Index: Int,
) : CallbackEvent {

    companion object {
        enum class Source {
            Console, Client, API, Remote
        }

    }
}