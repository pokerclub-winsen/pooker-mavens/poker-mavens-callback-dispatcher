package dispatcher.event

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.time.LocalDateTime


@JsonIgnoreProperties(value = ["Password", "Event"])
data class TourneyKnockoutEvent(
    val Name: String,
    val Table: Int,
    val Player: String,
    val Place: Int,
    val Bounty: String,
    val Hand: String,
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    val Time: LocalDateTime,
    val Index: Int,
) : CallbackEvent