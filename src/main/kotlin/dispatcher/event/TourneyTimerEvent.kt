package dispatcher.event

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.time.LocalDateTime


@JsonIgnoreProperties(value = ["Password", "Event"])
data class TourneyTimerEvent(
    val Name: String,
    val Number: Int,
    val Timer: Timer,
    val Count: Int,
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    val Time: LocalDateTime,
    val Index: Int,
) : CallbackEvent {

    companion object {
        enum class Timer {
            RegOpens, LateRegEnds, RebuyEnds, AddOnEnds, NoShowsRemoved
        }
    }

}