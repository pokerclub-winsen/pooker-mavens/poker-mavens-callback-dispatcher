package dispatcher.web

import com.fasterxml.jackson.databind.ObjectMapper
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.ApplicationEventPublisher
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.util.logging.Level
import java.util.logging.Logger


@RestController
class CallbackController(
    @Value("\${password}") private val password: String,
    private val objectMapper: ObjectMapper,
    private val publisher: ApplicationEventPublisher
) {

    companion object {
        private val logger = Logger.getLogger(this::class.java.name)
    }

    @PostMapping("/")
    fun handleCallbackRequest(@RequestParam params: Map<String, String>) {
        println("****** $params")

        if (params["Password"] == password) {
            val eventStr = params["Event"]
            if (eventStr != null) {
                try {
                    val clazz = Class.forName("dispatcher.event.${eventStr}Event")
                    publisher.publishEvent(objectMapper.convertValue(params, clazz))
                } catch (e: Exception) {
                    logger.log(Level.WARNING, "Event handling for params $params failed", e)
                }
            }
        } else {
            logger.fine("Received Mavens callback event with invalid password")
        }
    }
}